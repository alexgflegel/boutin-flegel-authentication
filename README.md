# BoutinFlegel Authentication README #

### What is this repository for? ###

* Managing authentication and Single Sign On across Single Page Applications

### How do I get set up? ###

* Visual Studio 2019
* SQL Server
* NuGet and MyGet are used for package management

### Contribution guidelines ###

* Learn [Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Other guidelines

### Who do I talk to? ###

* Alexander Boutin Flegel [alexander.flegel@omnilogic.net]
