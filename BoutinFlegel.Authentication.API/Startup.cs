﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BoutinFlegel.Authentication.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using OpenIddict.Abstractions;

namespace BoutinFlegel.Authentication.API
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors();
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

			services.AddDbContext<AuthenticationContext>(options =>
			{
				options.UseSqlServer(Configuration.GetConnectionString("AuthenticationContext"));
				//options.UseNpgsql(Configuration.GetConnectionString("AuthenticationPostgresContext"));

				options.UseOpenIddict();
			});

			// Register the Identity services.
			services.AddIdentity<ApplicationUser, IdentityRole>()
				.AddEntityFrameworkStores<AuthenticationContext>()
				.AddDefaultTokenProviders();

			services.Configure<IdentityOptions>(options =>
			{
				options.ClaimsIdentity.UserNameClaimType = OpenIddictConstants.Claims.Name;
				options.ClaimsIdentity.UserIdClaimType = OpenIddictConstants.Claims.Subject;
				options.ClaimsIdentity.RoleClaimType = OpenIddictConstants.Claims.Role;
			});

			services.AddOpenIddict().AddCore(options =>
			{
				// Register the Entity Framework stores.
				options.UseEntityFrameworkCore().UseDbContext<AuthenticationContext>();
			})
			.AddServer(options =>
			{
				options.UseMvc();
				options.EnableTokenEndpoint("/authorization/exchange");

				options.AllowPasswordFlow()
					.AllowRefreshTokenFlow();

				options.EnableRequestCaching();

				options.RegisterScopes(
					OpenIddictConstants.Scopes.Email,
					OpenIddictConstants.Scopes.Profile,
					OpenIddictConstants.Scopes.Roles);

				options.DisableHttpsRequirement();
				options.AcceptAnonymousClients();
			})
			.AddValidation();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, AuthenticationContext authenticationContext, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
				authenticationContext.Initialize(userManager, roleManager);
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseCors(builder =>
			{
				builder.WithOrigins("http://localhost:5055");
				builder.AllowAnyHeader();
				builder.AllowAnyMethod();
			});

			app.UseAuthentication();

			app.UseHttpsRedirection();
			app.UseMvc();
		}
	}
}
