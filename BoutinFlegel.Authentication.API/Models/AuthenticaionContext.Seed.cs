﻿namespace BoutinFlegel.Authentication.Models
{
	using Microsoft.AspNetCore.Identity;
	using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
	using Microsoft.EntityFrameworkCore;
	using System;
	using System.Linq;

	public partial class AuthenticationContext
	{
		public void Initialize(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			if (Database.EnsureCreated())
			{
				SeedData(userManager, roleManager);
			}
		}

		private void SeedData(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager)
		{
			if (!roleManager.RoleExistsAsync("Administrator").Result)
			{
				var role = new IdentityRole
				{
					Name = "Administrator"
				};
				_ = roleManager.CreateAsync(role).Result;
			}
		}
	}
}
